package Q2;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Runner {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        double basic, balanceOfCompanyBankAc;

        System.out.print("Enter basic salary of the lowest grade: ");
        basic = sc.nextDouble();
        while (basic < 0) {
            System.out.print("Basic can not be negative number \n");
            System.out.print("Enter basic salary of the lowest grade Again: ");
            basic = sc.nextDouble();
        }
        System.out.print("Enter balance of the company bank account: ");
        balanceOfCompanyBankAc = sc.nextDouble();
        while (balanceOfCompanyBankAc < 0) {
            System.out.print("Balance of company can not be negative number \n");
            System.out.print("Balance of company can not be negative number Again: ");
            balanceOfCompanyBankAc = sc.nextDouble();
        }
        BankAccount companyAccount = createCompanyAccount();
        double balance;
        balance = addingBalance(balanceOfCompanyBankAc, companyAccount);
        List<Employee> employeeList = loadEmployee();
        double total = calculateTotalSalary(employeeList, basic);
        while (total > balance) {
            System.out.print("Error: It's exceeding your company bank account amount \n");
            System.out.print("Enter balance of the company bank account: ");
            balanceOfCompanyBankAc = sc.nextDouble();
            balance = addingBalance(balanceOfCompanyBankAc, companyAccount);
            total = calculateTotalSalary(employeeList, basic);
        }
        // printing employee
        printingEmployee((SavingsAccount) companyAccount, employeeList, total);
    }

    // Creating Employee Bank Account
    public static List<Employee> loadEmployee() {
        BankAccount employee1 = new SavingsAccount("Abdur Rahim", "Asia Bank", "CDA");
        BankAccount employee2 = new SavingsAccount("Faisal Ahmed", "Asia Bank", "CDA");
        BankAccount employee3 = new SavingsAccount("Asif mahmud", "Asia Bank", "CDA");
        BankAccount employee4 = new SavingsAccount("Ishmam Abir", "Asia Bank", "CDA");
        BankAccount employee5 = new SavingsAccount("Tasbir Ahmed", "Asia Bank", "CDA");
        BankAccount employee6 = new SavingsAccount("Ahmed Abdullah", "Asia Bank", "CDA");
        BankAccount employee7 = new SavingsAccount("karim", "Asia Bank", "CDA");
        BankAccount employee8 = new SavingsAccount("Abir khan", "Asia Bank", "CDA");
        BankAccount employee9 = new SavingsAccount("Riad", "Asia Bank", "CDA");
        BankAccount employee10 = new SavingsAccount("Ripon", "Asia Bank", "CDA");
        //Creating Employee
        List<Employee> employeeList = new ArrayList();
        employeeList.add(new GradeOneEmployee("Abdur Rahim", "Ctg", "01866331195", employee1));
        employeeList.add(new GradeTwoEmployee("Faisal Ahmed", "Ctg", "01866331195", employee2));
        employeeList.add(new GradeThreeEmployee("Asif mahmud", "Ctg", "01866331195", employee3));
        employeeList.add(new GradeThreeEmployee("Ishmam Abir", "Ctg", "01866331195", employee4));
        employeeList.add(new GradeFourEmployee("Tasbir Ahmed", "Ctg", "01866331195", employee5));
        employeeList.add(new GradeFourEmployee("Ahmed Abdullah", "Ctg", "01866331195", employee6));
        employeeList.add(new GradeFiveEmployee("karim", "Ctg", "01866331195", employee7));
        employeeList.add(new GradeFiveEmployee("Abir khan", "Ctg", "01866331195", employee8));
        employeeList.add(new GradeSixEmployee("Riad", "Ctg", "01866331195", employee9));
        employeeList.add(new GradeSixEmployee("Ripon", "Ctg", "01866331195", employee10));
        return employeeList;
    }

    private static void printingEmployee(SavingsAccount companyAccount, List<Employee> employeeList, double total) {
        for (Employee emp : employeeList) {
            emp.getBankAccount().deposit(emp.getSalary());
            SavingsAccount.transfer(companyAccount, (SavingsAccount) emp.getBankAccount(), emp.getSalary());
            emp.displayEmployeeInfo();
        }
        System.out.println("Total Paid Salary: " + total);
        System.out.println("Remaining Balance of Company: " + companyAccount.getBalance());
    }

    public static BankAccount createCompanyAccount() {
        BankAccount companyAccount = new SavingsAccount("Company Account", "Asia Bank", "CDA");
        return companyAccount;
    }

    public static double addingBalance(double amount, BankAccount account) {
        return account.deposit(amount);
    }

    public static double calculateTotalSalary(List<Employee> employeeList, double basic) {
        double total = 0;
        for (Employee emp : employeeList) {
            double salary = calculateSalary(basic, emp);
            total += salary;
        }
        return total;
    }

    public static double calculateSalary(double basic, Employee emp) {
        double salary = 0;
        salary = emp.calculateSalary(basic);
        return salary;
    }
}
