package Q2;

public class GradeThreeEmployee extends Employee {

    public GradeThreeEmployee(String name, String address, String mobileNo, BankAccount bankAccount) {
        super(name, Grade.grade_three, address, mobileNo, bankAccount);
    }

    @Override
    public double calculateSalary(double basic) {
        double increment = 5000;
        return super.calculateSalary(basic + (3 * increment));
    }
}
