package Q2;

public class GradeFiveEmployee extends Employee {

    public GradeFiveEmployee(String name, String address, String mobileNo, BankAccount bankAccount) {
        super(name,Grade.grade_five, address, mobileNo, bankAccount);
    }
    @Override
    public double calculateSalary(double basic) {
        double increment = 5000;
        return super.calculateSalary(basic +  increment);
    }
}
