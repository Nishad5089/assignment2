package Q2;

public class GradeTwoEmployee extends Employee {

    public GradeTwoEmployee(String name, String address, String mobileNo, BankAccount bankAccount) {
        super(name, Grade.grade_two, address, mobileNo, bankAccount);
    }

    @Override
    public double calculateSalary(double basic) {
        double increment = 5000;
        return super.calculateSalary(basic + (4 * increment));
    }
}
