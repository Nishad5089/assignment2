package Q2;

public class GradeOneEmployee extends Employee {

    public GradeOneEmployee(String name, String address, String mobileNo, BankAccount bankAccount) {
        super(name, Grade.grade_one, address, mobileNo, bankAccount);
    }

    @Override
    public double calculateSalary(double basic) {
        double increment = 5000;
        return super.calculateSalary(basic + (5 * increment));
    }
}
