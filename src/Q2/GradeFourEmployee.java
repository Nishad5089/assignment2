package Q2;

public class GradeFourEmployee extends Employee {

    public GradeFourEmployee(String name, String address, String mobileNo, BankAccount bankAccount) {
        super(name, Grade.grade_four, address, mobileNo, bankAccount);
    }

    @Override
    public double calculateSalary(double basic) {
        double increment = 5000;
        return super.calculateSalary(basic + (2 * increment));
    }
}
