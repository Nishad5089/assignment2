package Q2;

public class GradeSixEmployee extends Employee {

    public GradeSixEmployee(String name, String address, String mobileNo, BankAccount bankAccount) {
        super(name, Grade.grade_six, address, mobileNo, bankAccount);
    }

    @Override
    public double calculateSalary(double basic) {
        double increment = 5000;
        return super.calculateSalary(basic);
    }
}
