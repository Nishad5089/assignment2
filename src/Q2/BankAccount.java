package Q2;

public abstract class BankAccount {
    protected String bankName;
    protected String branchName;
    protected double balance;

    protected BankAccount(String bankName, String branchName) {
        this.bankName = bankName;
        this.branchName = branchName;
    }

    protected double getBalance() {
        return balance;
    }

    public double deposit(double amount) {
        balance += amount;
        return balance;
    }

    public double withdraw(double amount) {
        balance -= amount;
        return balance;
    }
}
