package Q2;

public class SavingsAccount extends BankAccount {
    private String accountType;
    private String accountName;
    private long accountNumber;

    public SavingsAccount(String accountName, String bankName, String branchName) {
        super(bankName, branchName);
        this.accountNumber = (long) Math.floor(Math.random() * 9000000000L) + 1000000000L;
        this.accountType = "Savings";
        this.accountName = accountName;
    }

    @Override
    public double deposit(double amount) {
        if (amount < 0) {
            System.out.println("Error: Deposit amount is invalid.");
        } else {
            super.deposit(amount);
        }
        return balance;
    }

    @Override
    public double withdraw(double amount) {
        if (amount < 0) {
            System.out.println("Error: Withdraw amount is invalid.");
        } else if (balance - amount >= 0) {
            super.withdraw(amount);
        } else {
            System.out.println("Error: Insufficient Balance.");
        }
        return balance;
    }

    public static void transfer(SavingsAccount from, SavingsAccount to, double amount) {
        if (amount < 0) {
            System.out.println("Error: Transfer amount is invalid.");
        } else {
            from.withdraw(amount);
            to.deposit(amount);
        }
    }
}
