package Q2;
public abstract class Employee {
    protected String name;
    protected String address;
    protected String mobileNo;
    protected BankAccount bankAccount;
    protected double salary;
    protected Grade grade;

    public Employee(String name,Grade grade, String address, String mobileNo, BankAccount bankAccount) {
        this.name = name;
        this.address = address;
        this.mobileNo = mobileNo;
        this.bankAccount = bankAccount;
        this.grade = grade;
    }

    public double calculateSalary(double basic) {
        double houseRent = 0, medicalAllowance = 0;
        houseRent = 20 * basic / 100;
        medicalAllowance = 15 * basic / 100;
        salary = basic + houseRent + medicalAllowance;
        return salary;
    }

    public BankAccount getBankAccount() {
        return bankAccount;
    }

    public double getSalary() {
        return salary;
    }
    public void displayEmployeeInfo(){
        System.out.println("Name: "+name + " Rank: "+grade + " Salary "+ salary);
    }
}
