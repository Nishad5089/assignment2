package Q1;
import java.util.Scanner;

public class Runner {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int dayNumber = sc.nextInt();
        if (dayNumber > 0 && dayNumber < 32) {
            int mod = dayNumber % 7;
            switch (mod) {
                case 0:
                    System.out.println(Day.Saturday);
                    break;
                case 1:
                    System.out.println(Day.Sunday);
                    break;
                case 2:
                    System.out.println(Day.Monday);
                    break;
                case 3:
                    System.out.println(Day.Tuesday);
                    break;
                case 4:
                    System.out.println(Day.Wednesday);
                    break;
                case 5:
                    System.out.println(Day.Thursday);
                    break;
                case 6:
                    System.out.println(Day.Friday);
                    break;
                default:
                    System.out.println("Not Found");
            }
        } else {
            System.out.println("Invalid Input");
        }

    }
}
